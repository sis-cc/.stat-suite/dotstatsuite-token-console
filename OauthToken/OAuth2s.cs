﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OauthToken
{
    class OAuth
    {
	    private static Lazy<HttpClient> Client = new Lazy<HttpClient>(() => new HttpClient(new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }));
	    private static Dictionary<string, OAuth> _authDict = new Dictionary<string, OAuth>();
        
	    public TokenResponse Tokens { get; private set; }

	    private string clientID;
        private string clientSecret;
        private string baseUri;
        private string redirectUri;
        private ConfigResponse config;

        public static OAuth GetOAuth(string baseUri, string clientID, string clientSecret=null)
        {
	        return new OAuth(baseUri, clientID, clientSecret);
        }

        private OAuth(string baseUri, string clientID, string clientSecret=null)
        {
	        this.clientID = clientID;
	        this.clientSecret = clientSecret;
	        this.baseUri = baseUri.TrimEnd('/') + '/';
        }

        public void Authorize(Action onComplete, Action<Exception> onError, CancellationToken token) {

	        Authenticate().ContinueWith(t => {
	                if (t.Exception != null)
	                {
		                onError(t.Exception);
	                }
	                else
	                {
						this.SetTokens(t.Result);
						onComplete();
	                }
	            }
			);
        }

        async Task<TokenResponse> Authenticate()
        {
	        config = config ?? await Get<ConfigResponse>(this.baseUri + ".well-known/openid-configuration", false);
	        
	        // Generates state and PKCE values.
            string state = RandomDataBase64Url(32);
            string code_verifier = RandomDataBase64Url(32);
            string code_challenge = Base64UrlencodeNoPadding(Sha256(code_verifier));
            const string code_challenge_method = "S256";

            // Creates a redirect URI using an available port on the loopback address.
            this.redirectUri = $"http://{IPAddress.Loopback}:{GetRandomUnusedPort()}/";

            // Creates an HttpListener to listen for requests on that redirect URI.
            var http = new HttpListener();
            http.Prefixes.Add(this.redirectUri);
            http.Start();

            // Creates the OAuth 2.0 authorization request.
            string authorizationRequest = string.Format("{0}?response_type=code&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}&scope=offline_access",
                config.AuthEndpoint,
                System.Uri.EscapeDataString(this.redirectUri),
                clientID,
                state,
                code_challenge,
                code_challenge_method
             );

            // Opens request in the browser.
            System.Diagnostics.Process.Start(authorizationRequest);

            // Waits for the OAuth authorization response.
            var context = await http.GetContextAsync();

            // Sends an HTTP response to the browser.
            var response = context.Response;
            string responseString = string.Format("<html><head></head><body>Please return to the app.</body></html>");
            var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;
            Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) => {
                responseOutput.Close();
                http.Stop();
            });

            // Checks for errors.
            if (context.Request.QueryString.Get("error") != null) {
                var message = String.Format("OAuth authorization error: {0}.", context.Request.QueryString.Get("error"));
                throw new Exception(message);
            }
            if (context.Request.QueryString.Get("code") == null
                || context.Request.QueryString.Get("state") == null) {
                var message = "Malformed authorization response. " + context.Request.QueryString;
                throw new Exception(message);
            }

            // extracts the code
            var code = context.Request.QueryString.Get("code");
            var incoming_state = context.Request.QueryString.Get("state");

            // Compares the receieved state to the expected value, to ensure that
            // this app made the request which resulted in authorization.
            if (incoming_state != state) {
                var message = String.Format("Received request with invalid state ({0})", incoming_state);
                throw new Exception(message);
            }

            // Starts the code exchange at the Token Endpoint.
            return await CodeExchange(code, code_verifier);
        }

        async Task<TokenResponse> CodeExchange(string code, string code_verifier)
        {
	        return await Post<TokenResponse>(config.TokenEndpoint, new []
	        {
				new KeyValuePair<string, string>("grant_type", "authorization_code"),
				new KeyValuePair<string, string>("code", code),
				new KeyValuePair<string, string>("code_verifier", code_verifier),
				new KeyValuePair<string, string>("client_id", this.clientID),
				new KeyValuePair<string, string>("redirect_uri", this.redirectUri)
	        });
        }

        private void SetTokens(TokenResponse tokens)
        {
	        this.Tokens = tokens;
        }

        public async Task<TokenResponse> RefreshTokens()
        {
            var tokens = await Post<TokenResponse>(config.TokenEndpoint, new[]
                {
                    new KeyValuePair<string, string>("grant_type", "refresh_token"),
                    new KeyValuePair<string, string>("client_id", this.clientID),
                    new KeyValuePair<string, string>("refresh_token", Tokens.RefreshToken)
                },
                false
            );

            SetTokens(tokens);

            return tokens;
        }

        private async Task<T> Get<T>(string url, bool withAuth) where T:class
        {
	        using (var requestMessage = GetMessage(HttpMethod.Get, url))
	        {
		        var response    = await Client.Value.SendAsync(requestMessage);
		        return await response.Content.ReadAsAsync<T>();
	        }
        }

        private async Task<T> Post<T>(string url, IEnumerable<KeyValuePair<string, string>> @params, bool withAuth=true) where T:class
        {
	        using (var requestMessage = GetMessage(HttpMethod.Post, url))
	        {
		        requestMessage.Content = new FormUrlEncodedContent(@params);
		        var response = await Client.Value.SendAsync(requestMessage);
		        return await response.Content.ReadAsAsync<T>();
	        }
        }

        private HttpRequestMessage GetMessage(HttpMethod method, string url, bool withAuth = true)
        {
	        var message = new HttpRequestMessage(method, url);

	        if (withAuth)
	        {
		        message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Tokens?.AccessToken);
	        }

	        if (this.redirectUri != null)
	        {
				message.Headers.Add("Origin", this.redirectUri);
	        }

			message.Headers.Add("Accept", "application/json");

	        return message;
        }

        // ref http://stackoverflow.com/a/3978040
        private static int GetRandomUnusedPort() {
	        var listener = new TcpListener(IPAddress.Loopback, 0);
	        listener.Start();
	        var port = ((IPEndPoint)listener.LocalEndpoint).Port;
	        listener.Stop();
	        return port;
        }

        /// <summary>
        /// Returns URI-safe data with a given input length.
        /// </summary>
        /// <param name="length">Input length (nb. output will be longer)</param>
        /// <returns></returns>
        private static string RandomDataBase64Url(uint length) {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);
            return Base64UrlencodeNoPadding(bytes);
        }

        /// <summary>
        /// Returns the SHA256 hash of the input string.
        /// </summary>
        /// <param name="inputStirng"></param>
        /// <returns></returns>
        private static byte[] Sha256(string inputStirng) {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();
            return sha256.ComputeHash(bytes);
        }

        /// <summary>
        /// Base64url no-padding encodes the given input buffer.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private static string Base64UrlencodeNoPadding(byte[] buffer) {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }

        public class ConfigResponse
        {
	        [JsonProperty("authorization_endpoint")]
			public string AuthEndpoint { get; set; }

			[JsonProperty("token_endpoint")]
	        public string TokenEndpoint { get; set; }

	        [JsonProperty("userinfo_endpoint")]
	        public string UserinfoEndpoint { get; set; }

	        [JsonProperty("end_session_endpoint")]
	        public string LogoutEndpoint { get; set; }
        }

        public class TokenResponse
        {
	        [JsonProperty("access_token")]
			public string AccessToken { get; set; }

			[JsonProperty("expires_in")]
			public int AccessExpire { get; set; }

			[JsonProperty("refresh_token")]
			public string RefreshToken { get; set; }

			[JsonProperty("refresh_expires_in")]
			public int RefreshExpire { get; set; }
        }
    }
}
