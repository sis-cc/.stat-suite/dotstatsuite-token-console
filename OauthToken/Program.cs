﻿using System;
using System.Threading;

namespace OauthToken
{
    class Program
    {
        private static OAuth oAuth;
        private static CancellationTokenSource tokenSource;

        static int Main(string[] args)
        {
            if (args.Length != 2)
            {
                return ShowUsageMessage();
            }

            oAuth = OAuth.GetOAuth(args[0], args[1]);

            using (tokenSource = new CancellationTokenSource())
            {
                oAuth.Authorize(OnComplete, OnError, tokenSource.Token);

                Console.WriteLine("Please login in your browser...");

                while (!tokenSource.Token.IsCancellationRequested)
                {
                    Thread.Sleep(500);
                }
            }

            return 0;
        }

        static int ShowUsageMessage()
        {
            Console.WriteLine("Usage: OauthToken.exe [realmUrl] [clientId]");
            return -1;
        }

        static void OnComplete()
        {
            Console.WriteLine($"accessToken: {oAuth.Tokens.AccessToken}");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine($"refreshToken: {oAuth.Tokens.RefreshToken}");

            tokenSource.Cancel();
        }

        static void OnError(Exception e)
        {
            Console.WriteLine(e.Message);

            tokenSource.Cancel();
        }
    }
}
