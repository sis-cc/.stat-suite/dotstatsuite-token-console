# Tool

Console tool takes as parameters **realm/tenant base URL** and **client ID** invokes a browser where user performs login with any supported method, including ADFS. 
After successful login tool writes accessToken, refreshToken to the standard output. 

# Example usage

```
OauthToken.exe https://keycloak.siscc.org/auth/realms/OECD app
```

# Example workflow and output

1. Browser window opened after running a console command (The actual view can vary depending on Identity/Authentication service):

![login](img/browser_login.png)

2. Browser message after successful login:

![ok](img/browser_ok.png)

3. Tokens output example after successful login:

![out](img/output.png)

